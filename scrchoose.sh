#!/bin/bash


### scrchoose.sh
### (c) 2019 aslmx@gmx.de
###
### helps setting starting scrcpy (https://github.com/Genymobile/scrcpy) 
### dependices: (bash, adb,  yad) 
###
###


function install_yad()
{
read -p "I will apt-get install yad for you. Do you want to continue? Type Y or y to continue or any other key to abort: " -n 1 -r
echo ""   # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    sudo apt-get install yad
else
	exit 1
fi
}

## check dependencies
command -v yad &> /dev/null || { echo >&2 "I need yad installed."; install_yad || exit 1; }
command -v adb &> /dev/null || { echo >&2 "ADB is recommended."; useadb="no"; exit 1; }
command -v scrcpy &> /dev/null || { echo >&2 "scrcyp is required"; useadb="no";  exit 1; }

# Read Adb devices
# do not pipe adb devices into while loop, as of:
# https://unix.stackexchange.com/questions/402750/modify-global-variable-in-while-loop
# check if useadb was set first (if it was set then probably to "no", if true skip reading device list
if [ -z "$useadb" ]; then
	while read line; do

    t_serial=`echo $line | awk -F' ' '{print $1}'`
    t_device=`echo $line | awk -F' ' '{$1=$2=""; print $0}'`

    echo "Adding Device $t_device with Serial: $t_serial"
 	echo "adding $NEWLINE $line"

	if [ -z "$T_devices" ]; then
        T_devices="$t_serial;$t_device" 
	else
        T_devices="$T_devices;$t_serial;$t_device" 
	fi

	done <<<$(adb devices -l | grep -i -v list)

echo -e $T_devices

fi

device=`echo $T_devices | awk -F ';' '{
    #print NR; # Print Record Number 
    for(i=1;i<=NF;i++){
        print $i; # Print Each Column separeted by Default EOL
    }
}' | yad --width=600 --height=400  --title="Choose ADB Target" \
            --separator="" --list --column="Serial" --column="Name" --print-column=1`

if [ $? -eq 0 ]; then
    echo "Starting scrcpy for: $device"
    scrcpy -s $device
else
	echo "Abort..."
fi
