# scrchoose.sh

A tiny yad powered gui to start scrcpy when multiple devices are available via adb.

![scrchoose.sh screenshot](scrchoose.png)

## Getting Started

dumpstarter.sh helps you setup a tcpdump in order to pipe the dump into wireshark. Everything that it does can be done without it, but it makes it a bit easier and lets you choose from predefined filters or connect to different capture hosts easily.

### Prerequisites

There are a few dependencies

* [scrcpy](https://github.com/Genymobile/scrcpy)
* yad
* awk, sed
* adb


```
sudo apt-get install yad
```

### Install and run

Install dependencies (see above), then

```

git clone https://gitlab.com/aslmx/scrchoose.git
cd scrchoose
chmod +x scrchoose.sh
./scrchoose.sh

```

Optional: install symlink

```
sudo ln -s /home/user/tools/scrchoose/scrchoose.sh /usr/sbin/scrchoose
```

Optional: copy desktop file into applications
(Might depend on your distribution)

```
sudo cp scrchoose.desktop /usr/share/applications/
```

Simply run scrchoose.sh - either via commandline or via start menu (if you copied the .desktop file

scrchoose.sh has no commandline arguments.

## Limitations ##

todo

### Parsing ###

todo

## Misc

Feel free to comment / raise issues / praise ....

## Built With

* GNU nano
* love
* passion

## Authors

* aslmx@gmx.de

## License

This project is given to public domain.

## Acknowledgments

* Hat tip to anyone whose code was used
* https://gist.github.com/PurpleBooth/109311bb0361f32d87a2



